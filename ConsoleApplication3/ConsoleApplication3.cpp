﻿#include <iostream>
#include <cmath>


class Example
{
private:
	int a;
public:
	int GetA(int newA)
	{
		a = newA;
		return a;
	}
};

class Vector
{
public:
	Vector() :x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) :x(_x), y(_y), z(_z)
	{}
	double ModSumm()
	{
		s = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		return s;
	}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;

	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
	double s = 0;
};




int main()
{
	Example temp;
	std::cout << temp.GetA(8);
	Vector v(10, 10, 10);
	v.Show();

	std::cout << '\n' << v.ModSumm();;
}
